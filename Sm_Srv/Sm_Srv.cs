﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net.NetworkInformation;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Drawing;
using System.Drawing.Imaging;

namespace Sm_Srv
{
  public partial class Sm_Srv : ServiceBase
  {
    //const string constr = "Server=GASIS-HP2;Database=gasis;UId=www;Password=LetMeIn;Connection Timeout=5;";
    public static bool IsDebugComputer() { return Environment.MachineName == "DESKTOP-ESR8QOJ"; }
    private const string ConnectionString_Debug = "Server=DESKTOP-ESR8QOJ;Database=SmuOk;Trusted_Connection=True;";
    private const string ConnectionString_Prod = "Server=192.168.204.189\\SQLEXPRESS01;Database=SmuOk;Trusted_Connection=Yes;";
    public static string ConStr { get; } = IsDebugComputer() ? ConnectionString_Debug : ConnectionString_Prod;
    public static string SaveFilesTo = "";

    public const string MyServerIp = "127.0.0.1";//"192.168.204.9";//\\192.168.204.9\foto_screen$
    //private StreamWriter file;
    private System.Timers.Timer timer;
    private string sIP = "";

    public Sm_Srv()
    {
      InitializeComponent();
    }

    private static Size GetTrueScreenSize(Screen screen)
    {
      //get system DPI
      var systemDPI = (int)Registry.GetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "LogPixels", 96);

      using (var graphics = Graphics.FromHwnd(IntPtr.Zero))
      {
        //get graphics DPI
        var dpiX = graphics.DpiX;
        var dpiY = graphics.DpiY;

        //get true screen size
        var w = (int)Math.Round(screen.Bounds.Width * systemDPI / dpiX);
        var h = (int)Math.Round(screen.Bounds.Height * systemDPI / dpiY);

        return new Size(w, h);
      }
    }

    public static bool MyMakeScreenshot(string FullPath = "")
    {
      //get true screen size
      var size = GetTrueScreenSize(Screen.PrimaryScreen);

      //create bitmap and make screenshot
      var bmp = new Bitmap(size.Width, size.Height);
      using (var graph = Graphics.FromImage(bmp))
        graph.CopyFromScreen(0, 0, 0, 0, size);

      //save
      bmp.Save(FullPath, ImageFormat.Png);
      return true;
    }

    protected override void OnStart(string[] args)
    {
      string s = Environment.MachineName.Replace('/','_').Replace('\\','_');
      SaveFilesTo = "D:\\~project\\94. СМУ 24\\SmuOK\\Sm_Srv\\tmp\\" + s ;// MyGetOneValue("select EOValue from _engOptions where EOName='ScreensFolder'");
      if (!Directory.Exists(SaveFilesTo)) Directory.CreateDirectory(SaveFilesTo);
      SaveFilesTo += "\\" + DateTime.Now.ToString("yyyy.MM.dd hh.mm.ss") + ".png";
      //File.Create(SaveFilesTo);
      MyMakeScreenshot(SaveFilesTo);
    }

    protected override void OnStop()
    {
      //TechLog("");
    }

    /*private void MyDBLog(string s)
    {
      MyExecute("insert into  '" + sIP + "','" + Environment.MachineName + "','" + s + "'");
    }*/
    public static void TechLog(string s)
    {
      MyExecute("insert into _engTechLog (ETLDBUser, ETLAction) values (-1," + MyES(s) + ")");
      return;
    }

    public static string MyES(object val, bool bForLike = false, bool clear_as_null = false)
    {
      string s = val.ToString();
      if (val.GetType().ToString() == "System.Decimal") s = s.Replace(',', '.');
      //заменяем пробел на % для LIKE
      s = s.Replace("\r", " ");//.Replace("\n", " ");
      while (s.Contains(" \n") || s.Contains("\n ") || s.Contains("\n\n") || s.Contains("  ")) s = s.Replace(" \n", "\n").Replace("\n ", "\n").Replace("\n\n", "\n").Replace("  ", " ");
      s = s.Replace(" .", ".").Replace(" ,", ",");
      s = s.Trim(new char[] { ' ', '\n' });
      if (clear_as_null && s == "" & bForLike == false)
        return "null";
      if (bForLike)
        return "N'%" + s.Replace("'", "''").Replace(" ", "%") + "%'";
      else
        return "N'" + s.Replace("'", "''") + "'";
    }

    static void MyExecute(string sQuery)
    {
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand Com = new SqlCommand(sQuery, con))
          {
            Com.ExecuteNonQuery();
          }
        }
      }
      catch (System.Exception ex)
      {
        //
      }
    }

    public static string MyGetOneValue(string sQuery)
    {
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand Com = new SqlCommand(sQuery, con))
          {
            using (SqlDataReader r = Com.ExecuteReader())
            {
              if (r.HasRows)
              {
                r.Read();
                return (r[0].ToString());
              }
              else
              {
                return ("");
              }
            }
          }
        }
      }
      catch (System.Exception ex)
      {
        //MyConsoleWriteErrorLine(ex.Message);
        return ("");
      }
    }

    public static bool PingHost(string nameOrAddress = MyServerIp)
    {
      Ping pingSender = new Ping();
      PingOptions options = new PingOptions();

      options.DontFragment = true;

      string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
      byte[] buffer = Encoding.ASCII.GetBytes(data);
      int timeout = 350; //in ms
      PingReply reply = pingSender.Send(nameOrAddress, timeout, buffer, options);

      return reply.Status == IPStatus.Success;
    }

  }
}
