﻿using System;
using System.Configuration.Install;
using System.ServiceProcess;
using System.Windows.Forms;

namespace Sm_Srv
{
  static class Program
  {
    /// <summary>
    /// Тут установка, удаление и запуск сервиса. Во втором файле вся жизнь.
    /// </summary>
    static void Main(string[] args)
    {
      try
      {
        if (Environment.UserInteractive)
        {
          string parameter = string.Concat(args);
          if (parameter == "")
          {
            MessageBox.Show("Запустите с одним из параметров:\n\n    --install\n    --uninstall");
            return;
          }
          if (MessageBox.Show(parameter + " ?","Забор покрасьте",MessageBoxButtons.YesNo)!=DialogResult.Yes) return;
          switch (parameter)
          {
            case "--install":
              if (Sm_Srv.PingHost())
              {
                MessageBox.Show("Server ping ok, installing...");
                ManagedInstallerClass.InstallHelper(new[] { System.Reflection.Assembly.GetExecutingAssembly().Location });
                ServiceController controller1 = new ServiceController("Sm_Srv");
                if (controller1.Status == ServiceControllerStatus.Stopped) controller1.Start();
                MessageBox.Show("Service installed.");
              }
              else MessageBox.Show("Can't ping target server: " + Sm_Srv.MyServerIp + "\n\nService was not installed.", "Error",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
              break;
            case "--uninstall":
              ServiceController controller = new ServiceController("Sm_Srv");
              if (controller.Status == ServiceControllerStatus.Stopped) controller.Stop();
              ManagedInstallerClass.InstallHelper(new[] { "/u", System.Reflection.Assembly.GetExecutingAssembly().Location });
              break;
          }
        }
        else
        {
          //MessageBox.Show("run");
          ServiceBase[] ServicesToRun;
          ServicesToRun = new ServiceBase[] { new Sm_Srv() };
          ServiceBase.Run(ServicesToRun);
        }
      }
      catch (SystemException ex)
      {
        MessageBox.Show("епта || "+ex.Message);
      }
      /*ServiceBase[] ServicesToRun;
      ServicesToRun = new ServiceBase[]{new Sm_Srv()};
      ServiceBase.Run(ServicesToRun);*/
    }
  }
}
