﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Win32;

namespace SmuHelper
{
  static class Excel
  {
    public static bool IsDebugComputer() { return Environment.MachineName == "DESKTOP-ESR8QOJ"; }
    private const string ConnectionString_Debug = "Server=DESKTOP-ESR8QOJ;Database=SmuOkk;Trusted_Connection=True;";
    private const string ConnectionString_Prod = "Server=SERVER-SMUOK\\SQLEXPRESS01;Database=SmuOk;Trusted_Connection=Yes;";
    public static string ConStr { get; } = IsDebugComputer() ? ConnectionString_Debug : ConnectionString_Prod;

    public static void TechLog(string s)
    {
      MyExecute("insert into _engTechLog (ETLDBUser, ETLAction) values (0," + MyES("[SmuHelper] " + s) + ")");
      return;
    }

    public static void MyExecute(string sQuery)
    {
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand Com = new SqlCommand(sQuery, con))
          {
            Com.ExecuteNonQuery();
          }
        }
      }
      catch (System.Exception ex) { }
    }

    public static Type MyExcelType()
    {
      Exception ex = new Exception("Не найдена известная версия Excel.");
      Type xls = Type.GetTypeFromProgID("Excel.Application") ?? Type.GetTypeFromProgID("Excel.Application.14") ?? Type.GetTypeFromProgID("Excel.Application.16");
      if (xls == null)
      {
        TechLog("Не найдена известная версия Excel.");
        throw ex;
      }
      return xls;
    }

    public static string MyES(object val)
    {
      string s = val.ToString();
      return "N'" + s.Replace("'", "''") + "'";
    }

    public static Object MyGetOneValue(string sQuery/*, out DBError Err, string sType = "String"*/)
    {
      Object ret;
      try
      {
        using (SqlConnection con = new SqlConnection(ConStr))
        {
          con.Open();
          using (SqlCommand com = new SqlCommand(sQuery, con))
          {
            ret = com.ExecuteScalar();
            return ret;
          }
        }
      }
      catch (Exception ex)
      {
        TechLog(ex.Message + "\n\n" + sQuery);
        return null;
      }
    }

    public static bool MyExcelOpenFile(string f, out dynamic oExcel, out dynamic oWorksheet)
    {

      Type ExcelType = Type.GetTypeFromProgID("Excel.Application");
      oExcel = Activator.CreateInstance(ExcelType);
      oExcel.Visible = false;
      oExcel.ScreenUpdating = false;
      oExcel.DisplayAlerts = false;

      try
      {
        RegistryKey rk = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Office\\14.0\\Excel\\Security", true);
        rk.SetValue("AccessVBOM", 1, RegistryValueKind.DWord);
        rk.SetValue("Level", 1, RegistryValueKind.DWord);
        rk.SetValue("VBAWarnings", 1, RegistryValueKind.DWord);
      }
      catch
      {
        oExcel.ScreenUpdating = true;
        oExcel.DisplayAlerts = true;
        oExcel.Quit();
        oWorksheet = null;
        GC.Collect();
        GC.WaitForPendingFinalizers();
        throw;
      }

      dynamic oBook = oExcel.Workbooks.Add();

      // макросом обходим проблему с именованным диапазоном при наличии в файле автофильтра
      var oModule = oBook.VBProject.VBComponents.Item(oBook.Worksheets[1].Name);
      var codeModule = oModule.CodeModule;
      var lineNum = codeModule.CountOfLines + 1;
      string sCode = "Public Sub myop1()\r\n";
      sCode += "  'MsgBox \"Hi from Excel\"" + "\r\n";
      sCode += "  Workbooks.Open \"" + f + "\"\r\n";
      sCode += "End Sub";

      codeModule.InsertLines(lineNum, sCode);
      oExcel.Run(oBook.Worksheets[1].Name + ".myop1");

      oExcel.Workbooks[1].Close();
      if (oExcel.Workbooks[1].Worksheets.Count > 1)
      {
        TechLog("В книге более 1 листа: " + f);
        oWorksheet = null;
        oExcel.Quit();
        return false;
      }

      oWorksheet = oExcel.Workbooks[1].Worksheets(1);
      return true;
    }

    public static void LoadExcelSheetToDB(string fname, DateTime dtFrom, DateTime dtTo)
    {
      string q = "delete from Order1S where datediff(d, " + MyES(dtFrom) + ", O1S6)>=0 and datediff(d, O1S6, " + MyES(dtTo) + ")>=0";
      MyExecute(q);

      string q_PrevInsert = "insert into Order1S (O1S5,O1S6,O1S7,O1S8,O1S9,O1S10,O1S11,O1S12,O1S13,O1S14,O1S15,O1S16,\n" +
        " O1S17,O1S18,O1S19,O1S20,O1S21,O1S22,O1S23,O1S24,O1S25,O1S26,O1S27,O1S28,O1S29,O1S30,O1S31,\n" +
        " O1S32,O1S33,O1S34,O1S35,O1S36,O1S37,O1S38,O1S39,O1S40,O1S41,O1S42,O1S43,O1S44,O1S45,O1S46,\n" +
        " O1S47,O1S48,O1S49,O1S50) values ";

      // делем наполнение запросов по 250 строк, как раньше, возвращаем пачку запросов.
      // в запросы кидаем все целиком, чтобы в одном месте править потом
      using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fname, false))
      {
        //Read the first Sheets 
        Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
        Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
        IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
        int r = 0, insert_count = 0;
        string s = "";
        DateTime MinDBDtValue = new DateTime(1900, 1, 1);
        foreach (Row row in rows)
        {
          r++;
          if (r <= 4) continue;
          IEnumerable<Cell> cells = row.Descendants<Cell>();
          if (cells.Count() < 50) continue;
          string s50 = GetXMLCellValue(doc, cells.ToList<Cell>()[49]);
          if (s50=="") continue;
          insert_count++;

          if (r % 50 == 0)
          {
            Console.SetCursorPosition(0, 2);
            Console.Write("Строка обрабатывается: " + r);
          }

          int c = 0;
          s += "\n(";
          foreach (Cell cell in cells)
          {
            c++;
            if (c < 5) continue;
            //s += ""; // тут свич и добавление
            string s_val = GetXMLCellValue(doc, cell);
            if (c == 5 || c == 16 || c == 22 || c == 24 || c == 32 || c == 36 || c == 40 || c == 41 || c == 43 || c == 45 || c == 49 || c == 50)
            { //num
              decimal rd;
              bool b = decimal.TryParse(s_val.Replace('.',','), out rd);
              if (b) s_val = "N'" + rd.ToString().Replace(',', '.') + "'";
              else s_val = "null";
            }
            else if (c == 6 || c == 23 || c == 25 || c == 31 || c == 35 || c == 44)
            { //dt
              DateTime dt = DateTime.MinValue;
              bool b = DateTime.TryParse(s_val, out dt);
              if (b)
              {
                if (DateTime.Compare(dt, MinDBDtValue) < 0) dt = MinDBDtValue;
                s_val = "N'" + dt.ToString() + "'";
              }
              else s_val = "null";
            }
            else
            { //string
              s_val = s_val == "" ? "null" : "N'"+s_val.Replace("'","''")+"'";
            }
            s += s_val + ",";
          }
          s = s.Substring(0, s.Length - 1) + "),";
          // тут проверка на 250
          if (insert_count % 250 == 0)
          { // накопилось 250 строк
            Console.SetCursorPosition(0, 1);
            Console.Write("Строк загружается: " + insert_count + " (" + r + ")");

            s = s.Substring(0, s.Length - 1); // убираем запятую
            s = q_PrevInsert + s + ";"; // запросна втавку 250 строк
            MyExecute(s);
            TechLog("Загружено строк: " + insert_count);
            s = "";
          }
        }

        if (insert_count % 250 != 0)
        { // перебрали все строки, создали запросы, к концу файла осталось не загружено <250 строк
          // если в файле кратно 250 строкам для загрузки, они загрузятся раньше
          s = s.Substring(0, s.Length - 1); // убираем запятую
          s = q_PrevInsert + s + ";"; // запрос на вcтавку <250 строк
          MyExecute(s);
          TechLog("Загружено строк: " + insert_count);
        }

      }
    }

    private static string GetXMLCellValue(SpreadsheetDocument doc, Cell cell) /* sType = [N, s, d]*/
    {
      string value = cell.CellValue?.InnerText ?? "";
      if (/*value != "" &&*/ cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
      {
        return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
      }
      return value;
    }


  }
}
