﻿CREATE FUNCTION [dbo].[fBudgNumLstMaker](@SFSupplyPID bigint)
returns nvarchar(MAX)
as
begin
	declare @TextProduct NVARCHAR(MAX);
	select @TextProduct = ISNULL(@TextProduct + ',','') + cast(b.BNumber as nvarchar) 
	FROM Budget b
	join BudgetFill bf on bf.BudgId = b.BId
	join SpecFill sf on sf.SFId = bf.SpecFillId
	where sf.SFSupplyPID = @SFSupplyPID
	return @TextProduct
end;
GO